#include <GL/gl.h>
#include <GL/glut.h>
#include <iostream>
#include <SOIL.h>
#include <stdio.h>
#include "objLoader.h"

// Ponteiro para o objeto que carregará o modelo 3D (formato OBJ).
objLoader *objDisco1;
objLoader *objBase;

unsigned int ViewPortWidth  = 512;
unsigned int ViewPortHeight = 512;
GLuint texture[13];
int numero =15;
int k =0;
GLfloat ambientBase[4]={1.0,1.0,1.0,1};

int LoadGLTextures()                                    // Converte jpg/png em arquivo de textura
{
//-----------------------------Base------------------------------------------------------
    /*
    texture[0] = SOIL_load_OGL_texture
        (
        "/home/jonathan/Documentos/CG/projetoFinalCG/objetos/texRed.jpg",//coloca aqui o caminho da textura
        SOIL_LOAD_AUTO,
        SOIL_CREATE_NEW_ID,
        SOIL_FLAG_INVERT_Y
        );

    if(texture[0] == 0)
        return false;



    glBindTexture(GL_TEXTURE_2D, texture[0]);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER,0.0);
	*/
	//----------------------------------Disco1----------------------------------
    texture[0]= SOIL_load_OGL_texture
	(
		"/home/jonathan/Documentos/CG/projetoFinalCG/objetos/txtpurple.jpg",
		SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_INVERT_Y
	);
	if (texture[0]== 0)
	    return false;
    glBindTexture(GL_TEXTURE_2D, texture[0]);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glLightModelf(GL_LIGHT_MODEL_LOCAL_VIEWER,0.0);
	
}
//-----------------------------------------------------------------------------
// Libera a memoria do objeto responsavel por guardar dados do modelo.
void display(void){
	glViewport(0, 0, ViewPortWidth, ViewPortHeight);

	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    //------------------------------Base----------------------------------------
	//glTranslatef(0.0,-0.8,0.0);
	glMatrixMode(GL_PROJECTION);
  	glLoadIdentity();
	gluPerspective(	55.0f, 								// angulo de abertura da camera
					(float)ViewPortWidth/ViewPortWidth, // aspecto da imagem
					1.0f, 								// near plane
					50.0f);								// far plane

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(	0.0f, 0.0f, 4.0f,	// posição da câmera
				0.0f, 0.0f, 0.0f,	// ponto para o qual a camera está olhando
				0.0f, 1.0f, 0.0f);	// vetor "up"

    glBindTexture(GL_TEXTURE_2D, texture[0]);

    glMaterialfv(GL_FRONT,GL_AMBIENT,ambientBase);
    glMaterialf(GL_FRONT, GL_SHININESS, 128.0f);
    glPushMatrix();
    //glTranslatef(-1.4,0.2,-80.0);
    glBegin(GL_TRIANGLES);
	for(int i=0; i<objBase->faceCount; i++){
            obj_face *o = objBase->faceList[i];

            glNormal3f( objBase->normalList[o->normal_index[0]]->e[0],
						objBase->normalList[o->normal_index[0]]->e[1],
						objBase->normalList[o->normal_index[0]]->e[2]);
            glTexCoord2f(objBase->textureList[o->texture_index[0]]->e[0],
                         objBase->textureList[o->texture_index[0]]->e[1]);
            glVertex3f( objBase->vertexList[o->vertex_index[0]]->e[0],          // primeiro vértice
						objBase->vertexList[o->vertex_index[0]]->e[1],
						objBase->vertexList[o->vertex_index[0]]->e[2]);


            glNormal3f(objBase->normalList[o->normal_index[1]]->e[0],
						objBase->normalList[o->normal_index[1]]->e[1],
						objBase->normalList[o->normal_index[1]]->e[2]);
            glTexCoord2f(objBase->textureList[o->texture_index[1]]->e[0],
                         objBase->textureList[o->texture_index[1]]->e[1]);
            glVertex3f(	objBase->vertexList[o->vertex_index[1]]->e[0],	        // segundo vértice
						objBase->vertexList[o->vertex_index[1]]->e[1],
						objBase->vertexList[o->vertex_index[1]]->e[2]);


            glNormal3f(objBase->normalList[o->normal_index[2]]->e[0],
						objBase->normalList[o->normal_index[2]]->e[1],
						objBase->normalList[o->normal_index[2]]->e[2]);
            glTexCoord2f(objBase->textureList[o->texture_index[2]]->e[0],
                         objBase->textureList[o->texture_index[2]]->e[1]);
            glVertex3f(	objBase->vertexList[o->vertex_index[2]]->e[0],	        // terceiro vértice
						objBase->vertexList[o->vertex_index[2]]->e[1],
						objBase->vertexList[o->vertex_index[2]]->e[2]);
        }

    glEnd();
    glPopMatrix();
	glFlush();
	glutSwapBuffers();
	glutPostRedisplay();

}
void printVector(obj_vector *v)
{
	printf("%.2f,", v->e[0] );
	printf("%.2f,", v->e[1] );
	printf("%.2f  ", v->e[2] );
}

void FreeMemFunc(void)
{
	std::clog << "Exiting...\n";

	if (!objBase)
		delete objBase;
	if (!objDisco1)
		delete objDisco1;
}

//-----------------------------------------------------------------------------
// Programa principal
int main(int argc, char **argv)
{
	//objData = new objLoader();			// cria o objeto que carrega o modelo
	//objData->load("monkey_head2.obj");	// a carga do modelo é indicada atraves do nome do arquivo. 
										// Neste caso, deve ser sempre do tipo OBJ.
	//char* textr1= {};
	//char* textr2= {"objetos/yellowdonnut.obj"};
	//os/reddonut.obj"};										
    //objDisco1 = new objLoader();
	//objDisco1->load("objetos/reddonut.mtl");//coloca o caminho do disco
	objBase = new objLoader();
	objBase->load("objetos/purpledonut.obj");
	// Habilite esta função se você deseja imprimir na tela dados do modelo
	// gerados durante a sua carga.
	//PrintModelInfo(objData);

	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(512, 512);
	glutInitWindowPosition(100,100);
	glutCreateWindow("Torres de Hanoi");
	//codigo de Alisson
	
	//cont.codigo
	if (!LoadGLTextures())
    {
        return printf("erro no loadGLTextures");
    }
    glEnable(GL_TEXTURE_2D);                            //ativa as texturas para o projeto
	glShadeModel(GL_SMOOTH);
	//glClearDepth(1.0f);
    //glEnable(GL_DEPTH_TEST);                            //ativa o teste de profundidade
    //glDepthFunc(GL_LEQUAL);
    //glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    //glEnable(GL_LIGHTING);                              //ativa a iluminação para o projeto
    //glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient);     //setup para a luz 1
	//glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse);
	//glLightfv(GL_LIGHT1, GL_SPECULAR, LightSpecular);
    //glLightfv(GL_LIGHT1, GL_POSITION, LightPosition1);
	//glEnable(GL_LIGHT1);                                //luz 1

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

	glutDisplayFunc(display);

	atexit(FreeMemFunc);

	glutMainLoop();

	return 0;
}

